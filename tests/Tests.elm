module Tests exposing (..)

import Expect
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "Test suite"
        [ test "Test" <|
            \_ ->
                (21 * 2)
                    |> Expect.equal 42
        ]
